<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class member extends CI_Controller {

	public function index(){

		$this->load->helper(array('form', 'url'));		
		$this->load->model('member_model');
		$this->load->library('session');
		
		$checkLoggedIn = $this->member_model->checkLoggedIn();

		if($checkLoggedIn == false){
			$this->load->view('user/login');
		}else{
			$data['username'] = $checkLoggedIn->username;
			$data['id'] = $checkLoggedIn->id;
			$data['email'] = $checkLoggedIn->email;
			$data['confirmed'] = $checkLoggedIn->confirmed;
			$this->load->view('member/index',$data);
		}
	}

	public function basicInfo(){
		$this->load->helper(array('form', 'url'));		
		$this->load->model('member_model');
		$this->load->library('session');

		$checkLoggedIn = $this->member_model->checkLoggedIn();
		if($checkLoggedIn == false){
			$this->load->view('user/login');
		}else{
			$data = $this->assignSessionToData($checkLoggedIn);
			if($this->uri->segment(3, 0) != FALSE)	$data['route'] = $this->uri->segment(3, 0);
			else $data['route'] = "";
			if($this->uri->segment(4, 0) != FALSE)	$data['param'] = $this->uri->segment(4, 0);
			else $data['param'] = "";
			$table = $this->find_route($data['route']);
			if($table != "no"){
				if($this->uri->segment(4, 0) != FALSE)	$data['param'] = $this->uri->segment(4, 0);
				else $data['param'] = $data['id'];
				if($this->input->post()){
					$insertData = $_POST; 
					if(!isset($insertData['id'])) 
						$insertData['id'] = "";
					if($this->member_model->updateData($insertData,$table,'id',$insertData['id']))
						$data['success'] = "Your entry has been saved successfully";
				}
				$userData = $this->member_model->selectUserData($table,$data['param'],'id');
				$data['form'] = $userData;
				$this->load->view('member/'.$data['route'].'Info',$data);	
		
			}else { /* 404 */}
	
	
		}
	}
	
	public function deleteRow(){
		$this->load->helper(array('form', 'url'));		
		$this->load->model('member_model');
		$this->load->library('session');

		$checkLoggedIn = $this->member_model->checkLoggedIn();
		if($checkLoggedIn == false){
			$this->load->view('user/login');
		}else{
			$data = $this->assignSessionToData($checkLoggedIn);
			if($this->uri->segment(3, 0) != FALSE)	$data['route'] = $this->uri->segment(3, 0);
			else $data['route'] = "";
			if($this->uri->segment(4, 0) != FALSE)	$data['param'] = $this->uri->segment(4, 0);
			else $data['param'] = "";
			$table = $this->find_route($data['route']);
			if($table != "no"){
				$data['success'] = $this->member_model->deleteRows($table,$data['param'],'parent_id',$data['id']);
						//$data['success'] = "Your entry has been saved successfully";
				
				$this->load->view('member/deleteInfo',$data);	
		
			}else { /* 404  */}
	
	
		}
	}
	
	public function viewInfo(){
		$this->load->helper(array('form', 'url'));		
		$this->load->model('member_model');
		$this->tableStyle();		
		$checkLoggedIn = $this->member_model->checkLoggedIn();
		if($checkLoggedIn == false){
			$this->load->view('user/login');
		}else{
			$data = $this->assignSessionToData($checkLoggedIn);
		if($this->uri->segment(3, 0) != FALSE){	$data['route'] = $this->uri->segment(3, 0);$route=$data['route']; }
		else {$data['route'] = "";}
				$table = $this->find_route($data['route']);
			if($table != "no"){
				$userData = $this->member_model->selectUserData($table,$data['id'],'parent_id',0);
				if($userData != FALSE )
					$data['userData'] = $userData;
				$basicElementView=$this->findTableHead($data['route']);
				$smallElements = $this->db->list_fields($table);
				$this->table->set_heading($basicElementView);
			if(isset($data['userData'])){	
				if($data['route'] == 'edu'){
					foreach($data['userData'] as $row){
						$this->table->add_row('<a href="'.site_url('member/basicInfo/edu/'.$row[$smallElements[0]]).'">Edit</a> / <a href="'.site_url('member/deleteRow/'.$route.'/'.$row[$smallElements[0]]).'">Delete</a>',$row[$smallElements[2]],$row[$smallElements[3]],$row[$smallElements[4]],$row[$smallElements[5]],$row[$smallElements[6]],$row[$smallElements[7]],$row[$smallElements[8]]);
					}
				} elseif($data['route'] == 'hobbies'){
					foreach($data['userData'] as $row){
						$this->table->add_row('<a href="'.site_url('member/basicInfo/hobbies/'.$row[$smallElements[0]]).'">Edit</a> / <a href="'.site_url('member/deleteRow/'.$route.'/'.$row[$smallElements[0]]).'">Delete</a>',$row[$smallElements[2]]);
					}
				} elseif($data['route'] == 'awards'){
					foreach($data['userData'] as $row){
						$this->table->add_row('<a href="'.site_url('member/basicInfo/awards/'.$row[$smallElements[0]]).'">Edit</a> / <a href="'.site_url('member/deleteRow/'.$route.'/'.$row[$smallElements[0]]).'">Delete</a>',$row[$smallElements[2]]);
					}
				}elseif($data['route'] == 'skills'){
					foreach($data['userData'] as $row){
						$this->table->add_row('<a href="'.site_url('member/basicInfo/skills/'.$row[$smallElements[0]]).'">Edit</a> / <a href="'.site_url('member/deleteRow/'.$route.'/'.$row[$smallElements[0]]).'">Delete</a>',$row[$smallElements[2]],$row[$smallElements[3]]);
					}
				}elseif($data['route'] == 'courses'){
					foreach($data['userData'] as $row){
						$this->table->add_row('<a href="'.site_url('member/basicInfo/courses/'.$row[$smallElements[0]]).'">Edit</a> / <a href="'.site_url('member/deleteRow/'.$route.'/'.$row[$smallElements[0]]).'">Delete</a>',$row[$smallElements[2]],$row[$smallElements[3]]);
					}
				}else {}
			}
				$data['dataTable'] = $this->table->generate(); 
			$this->load->view('member/'.$data['route'].'ViewInfo',$data);
			}else{
				$this->load->view('404');
			}
		}
	}
	/*
	 * Function to Generate the Resume PDF
	 *
	 *
	 */
	public function getPDF(){
//	require_once('./odt/library/odf.php');
//	$odf = new odf("./odt/tests/tutoriel1.odt");
	
	$this->load->helper(array('form', 'url'));		
		$this->load->model('member_model');
		$this->tableStyle();		
		$checkLoggedIn = $this->member_model->checkLoggedIn();
		if($checkLoggedIn == false){
			$this->load->view('user/login');
		}else{
			$data = $this->assignSessionToData($checkLoggedIn);
//			print_r($data);
//			echo "adsfsfasf";
		if($this->uri->segment(3, 0) != FALSE){	$data['route'] = $this->uri->segment(3, 0);$route=$data['route']; }
		else {$data['route'] = "";}

	$userData = $this->member_model->selectUserData('resume_personal',$data['id'],'id');
				
	$html="<html><head>";
	$html.="
		<style>
		*{
		}
		</style>
			</head>
		<body>
		";
			$html.= "<h2>".$userData['first_name']." ".$userData['last_name']."</h2>";

			$html.= "<h5> ".$userData['email']."</h5>";

			$html.= "<h5> ".$userData['phone_number']."</h5><hr/>
				<h3>CAREER OBJECTIVE</h3>
				".$userData['career_objective']."
				<br/>
				";

			$skills = $this->member_model->selectUserData('resume_skills',$data['id'],'parent_id',0);
			$html.="<h3> SKILLS </h3><table border=\"1\">";
			foreach($skills as $skill){
				
				$html.="<tr><td>".$skill['skill_type']." </td><td> ".$skill['skills']."</td></tr>";
			}
			$html.="</table>";
			
			$edus = $this->member_model->selectUserData('resume_edu',$data['id'],'parent_id',0);
			$html.="<h3> QUALIFICATION </h3><table border=\"1\">
				<tr><th> Year</th><th>Qualification</th>	<th>Description</th> <th>% </th></tr>
				";
			
			foreach($edus as $edu){
				$percent=($edu['marks_obtained'] / $edu['marks_total'])*100;
				$html.="<tr><td>".$edu['from']."-".$edu['to']."</td><td>".$edu['qualification']." </td><td> ".$edu['college']."</td><td>".$percent."</td></tr>";
			}
			$html.="</table>";
		
			
			$courses = $this->member_model->selectUserData('resume_courses',$data['id'],'parent_id',0);
			
			$html.="<h3> OTHER RELATED COURSES </h3><table border=\"1\">";
			foreach($courses as $course){
				
				$html.="<tr><td>".$course['duration']." </td><td> ".$course['course']."</td></tr>";
			}
			$html.="</table>";
			$html.="<h3>PERSONAL DETAILS</h3>
				<table border=\"1\">
				<tr>
				<td>Name</td><td>".$userData['first_name']. " " .$userData['last_name']."</td>      
				</tr><tr>
				<td>Gender</td><td>".$userData['sex']."</td> 
				</tr><tr>
				<td>Nationality</td><td> " .$userData['nationality']."</td> 
				</tr><tr>	
				<td>DOB</td><td>" .$userData['dob']."</td> 
				</tr><tr>
				
				<td>Martial Status</td><td>" .$userData['martial_status']."</td> 
				</tr><tr>
				<td>Languages Known</td><td> " .$userData['languages_known']."</td> </tr>
				</table>"
				;
		
			$hobbies = $this->member_model->selectUserData('resume_hobbies',$data['id'],'parent_id',0);
			
			$html.="<h3> HOBBIES </h3><ul>";
			foreach($hobbies as $hobby){
				
				$html.="<li>".$hobby['hobby']."</td></li>";
			}

			$html.="</ul>";
			$honors = $this->member_model->selectUserData('resume_awards',$data['id'],'parent_id',0);
			
			$html.="<h3> HONORS AND AWARDS </h3><ul>";
			foreach($honors as $honor){
				
				$html.="<li>".$honor['award']."</li>";
			}
	

			$html.="</ul>";

$html.="</body></html>";



				
echo $html;










			///////////////////////////////////////////////////////
	//		$userData = $this->member_model->selectUserData($table,$data['id'],'parent_id',0);
		/*	$odf->setVars('titre', 'PHP: Hypertext PreprocessorPHP: Hypertext Preprocessor');




			$message = "PHP (sigle de PHP: Hypertext Preprocessor), est un langage de scripts libre en exécutant les programmes en ligne de commande.";
	
			$odf->setVars('message', $message);
			// We export the file
			$odf->exportAsAttachedPDF();*/
		}
	}
	public function find_route($handle){
		$directions=array(
			'basic'  => 'resume_personal',
			'addEdu' => 'resume_edu',
			'edu' => 'resume_edu',
			'hobbies' => 'resume_hobbies',
			'awards' => 'resume_awards',
			'skills' => 'resume_skills',
			'courses' => 'resume_courses'
		);
		if(array_key_exists($handle, $directions))	
			return $directions[$handle];
		else
			return "no";
	}
	public function findTableHead($handle){
		$directions=array(
			'basic'  => 'resume_personal',
			'addEdu' => 'resume_edu',
			'edu' => array('Operation','Qualification','College','University','From','To','Marks Obtained','Marks Total'),
			'hobbies' => array('Operation','Hobby'),
			'awards' => array('Operation','Award'),
			'skills' => array('operation','Skill Type','Skills'),
			'courses' => array('operation','Duration','Course')
		);
		if(array_key_exists($handle, $directions))	
			return $directions[$handle];
		else
			return "no";
	}
	public function tableStyle(){
				$this->load->library('table');
		$tmpl = array (
                    'table_open'          => '<table border="1" cellpadding="4" cellspacing="2" width="100%">',
                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',
                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td>',
                    'cell_end'            => '</td>',
                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',
                    'table_close'         => '</table>'
              );
		$this->table->set_template($tmpl); 
	
	}
	public function assignSessionToData($checkLoggedIn){
		
		$this->load->library('session');
		$data['username'] = $checkLoggedIn->username;
		$data['id'] = $checkLoggedIn->id;
		$data['email'] = $checkLoggedIn->email;
		$data['confirmed'] = $checkLoggedIn->confirmed;
		return $data;
	}


}
?>
