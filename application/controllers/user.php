<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {

	 
	 public function index(){	 
		$this->load->helper(array('form', 'url'));
		$this->load->view('user/index');
	 }
	 

	public function signup(){
		$this->load->database();
		$this->load->helper(array('form', 'url'));

		if(!$_POST){			
			$this->load->view('user/register');

		}else{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|min_length[4]|max_length[18]');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[30]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
			
			if ($this->form_validation->run() == FALSE){ 
				echo validation_errors(); 
				$this->load->view('user/register');
			}else{
		
				$registerData = array(
        	       			'username' => $this->input->post('username'),
        	       			'password' => md5($this->input->post('password')),
        	       			'email' => $this->input->post('email'),
					'confirmed' => md5(microtime().$this->input->post('email'))
        	    		);
				if($this->db->insert('users', $registerData)){
					$this->load->library('email');
					$this->email->from('noreply@devplace.in', 'Vigas Deep');
					$this->email->to($registerData['email']); 
					$this->email->subject('Resume Maker Registration');
					$this->email->message('Welcome to our website, your account currently is not activated.

To activate your account please, please follow the hyperlink.


					. '.base_url().'index.php/user/confirm/'.$registerData['username'].'/'.$registerData['confirmed'].' 
					
					
Best Regards,
Resume Maker.'
					);	
					$this->email->send();
					// $this->email->print_debugger();	
					$data['register'] = 1;
//					$id = $this->db->insert_id();
//					$this->connect_user($this->input->post('username'),$id);
//					header('Location: '.base_url().'index.php/member');
					$this->load->view('user/register',$data);
				}
			}
		}
	}



	public function confirm()
	{
			$this->load->database();
	
		$this->load->helper('url');
		$data['user']=$this->uri->segment(3, 0);
		$data['code']=$this->uri->segment(4, 0);
		$query = $this->db->query("Select * from users where username='$data[user]' and confirmed='$data[code]'");
		if($query->num_rows() > 0){
			$row=$query->row();
			$data['row'] = $row;
			if($row->confirmed==$data['code']){
				if($this->db->query("update users set confirmed=1 where username='$data[user]' and confirmed='$data[code]'"))
				{
					$data['message']= "Your account has been activated, you can login now.";
				}
				else 
				{
					$data['message']= "Seems, there is a problem with your authentication token, either it is expired or its not valid. ";
				}
				
			}
		}
		$this->load->view('user/confirm',$data);
	}	
	//Login START
	public function login()
	{
	
		$this->load->helper('url');
		$this->load->database();
	
	
		if(!$_POST)
		{
			$this->load->view('user/login');
		}
		else
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$query = $this->db->query("Select * from users where username='$username'");
			if($query->num_rows() > 0){			
				$row = $query->row();
				if($row->confirmed != 1){
				$this->load->view('user/login');
				}
				if($row->password==md5($password)){
					$this->connect_user($row->username,$row->id);
					header('Location: '.base_url().'index.php/member');
				}else{
					$this->load->view('user/login');
				}
			}else{
				$this->load->view('user/login');
			}
			
		}
		
	}


	
	
	private function connect_user($user, $id){
		$this->load->library('session');
		$sessiondata = array(
                   'username'  => $user,
                   'key'     => md5($user."mBn".$id),
                   'logged_in' => TRUE
               	);

		$this->session->set_userdata($sessiondata);
	}

	public function logout(){ 
		$this->load->helper('url');
		$this->load->library('session');
		$this->session->sess_destroy();
		$this->load->view('user/logout');
	}
	
	///////////////////
}//END OF CLASS


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
