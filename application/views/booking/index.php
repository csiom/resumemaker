<?php $this->load->view('header'); ?>

<div id="container">
	<h1>Booking</h1>

    <div id="overviewBooking">
        <p class="overviewHeading"><?php echo $overview->title; ?></p>
        <span><?php echo $overview->description; ?></span>
    </div>
    <form name="contact-form" id="contact-form" method="post" action="<?php echo base_url('index.php/booking/confirm')?>">
<table width="30%" border="0" cellspacing="0" cellpadding="5" style="float:right;" id="bookingForm">

	<!-- TR -->
        <tr id="bookingRow">
          <td><label for="booking">Choose your booking</label><br/>
<div style="width:100%; text-align:center;margin-top:10px;" ><select name="booking" id="booking">
            <option value="" selected="selected"> - Choose Option -</option>
		<optgroup label="Transport">
            <option value="Bus">Bus</option>
		</optgroup>
            	<optgroup label="Appointment">
            <option value="Doctor">Doctor</option>
		</optgroup>
            	<optgroup label="Others">
            <option value="Accommodation">Accommodation</option>
            <option value="Restaurant">Restaurant</option>
		</optgroup>
          </select> 


	<div class="BusField"><table>
	<tr>
          <td><select name="company" id="company">
            <option value="" selected="selected">- Select Company -</option>
		<?php foreach ($busCompanies as $company):?>
		<option value="<?php echo $company->id; ?>"><?php echo $company->name; ?></option>
		<?php endforeach;?>
          </select></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
	<tr>
          <td><input type="text" class="date" name="date" id="date" value="Leaving date"/></td>
        
        </tr>
	<!-- / TR -->

	<!-- TR -->
	<tr>
          <td><select name="time" id="time">
                                <option value="" selected="selected">- Select Leaving Time -</option>
                                <option>00:00</option>
                                <option>01:00</option>
                                <option>02:00</option>
                                <option>03:00</option>
                                <option>04:00</option>
                                <option>05:00</option>
                                <option>06:00</option>
                                <option>07:00</option>
                                <option>08:00</option>
                                <option>09:00</option>
                                <option>10:00</option>
                                <option>11:00</option>
                                <option>12:00</option>
                                <option>13:00</option>
                                <option>14:00</option>
                                <option>15:00</option>
                                <option>16:00</option>
                                <option>17:00</option>
                                <option>18:00</option>
                                <option>19:00</option>
                                <option>20:00</option>
                                <option>21:00</option>
                                <option>22:00</option>
                                <option>23:00</option>
                            </select></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
	<tr>
          <td><select name="journey" id="journey">
            <option value="" selected="selected"> - Choose Journey -</option>
            <option value="BT -to- LL">BT -to- LL</option>
            <option value="LL -to- BT">LL -to- BT</option>
          </select></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
	<tr>
          <td><input name="passengers" id="passengers" type="text" value="No. of passengers"></td>
        </tr></table></div>




	<!-- / TR -->

	<div class="DoctorField"><table>
	<tr>
        <tr >
          <td><select name="hospital" id="hospital">
            <option value="" selected="selected"> - Select Hospital -</option>
		<?php foreach ($hospitals as $hospital):?>
		<option value="<?php echo $hospital->id; ?>"><?php echo $hospital->name; ?></option>
		<?php endforeach;?>
          </select></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
        <tr>
          <td><input type="text" class="date" name="Docdate" id="Docdate" value="Enter date"/></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
        <tr>
          <td><select name="Doctime" id="Doctime">
                                <option value="" selected="selected">- Select Time - </option>
                                <option>00:00</option>
                                <option>01:00</option>
                                <option>02:00</option>
                                <option>03:00</option>
                                <option>04:00</option>
                                <option>05:00</option>
                                <option>06:00</option>
                                <option>07:00</option>
                                <option>08:00</option>
                                <option>09:00</option>
                                <option>10:00</option>
                                <option>11:00</option>
                                <option>12:00</option>
                                <option>13:00</option>
                                <option>14:00</option>
                                <option>15:00</option>
                                <option>16:00</option>
                                <option>17:00</option>
                                <option>18:00</option>
                                <option>19:00</option>
                                <option>20:00</option>
                                <option>21:00</option>
                                <option>22:00</option>
                                <option>23:00</option>
                            </select></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
        <tr>
          <td><input type="text" class="validate[required,custom[onlyNumber]]" name="Docname" id="Docname" value="Enter Doctor's Name"/></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
        <tr id="doctorSelf">
          <td><select name="Docselfapp" id="Docselfapp">
		<option value="">Book for yourself?</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
          </select></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
        <tr class="PatientField">
          <td width="30%"><input type="text" class="validate[required,custom[onlyNumber]]" name="Docpatientname" id="Docpatientname" value="Patient's Name"/></td>
        </tr>
	<!-- / TR -->

	<!-- TR -->
        <tr class="PatientField">
          <td width="30%"><input type="text" class="validate[required,custom[onlyNumber]]" name="Docpatientage" id="Docpatientage" value="Patient's Age"/></td>
        </tr></table>
	<!-- / TR -->


</div>

<div class="AccommodationField">
    <table>
        <tr>
          <td><select name="accommodationType" id="accommodationType">
            <option value="">- Select Accommodation Type - </option>
            <?php foreach ($hotelTypes as $hotelType):?>
                <option ><?php echo $hotelType->name; ?></option>
            <?php endforeach;?>
            <option value="-1">No Preference</option>
          </select></td>
        </tr>

        <tr class="LocationField">
          <td width="30%">
            <select name="location" id="location">
                <option value="-1">- Select Location -</option>
                <?php foreach ($locations as $location):?>
                <option><?php echo $location->name; ?></option>
                <?php endforeach;?>
                <option value="-1">No Preference</option>
            </select>
          </td>
        </tr>

        <tr>
          <td><select name="accommodation" id="accommodation">
        <option value="">- Select Accommodation - </option>
          </select></td>
        </tr>

        <tr>
          <td><select name="roomType">
        <option value="">- Select Room Type - </option>
        <option >Standard</option>
        <option >Deluxe</option>
        <option >Superior</option>
        <option >Suite</option>
          </select></td>
        </tr>

        <tr>
          <td><input name="noOfPeopleHotel" value="Number of People"/></td>
        </tr>

        <tr>
          <td><input name="checkInDateHotel" value="Check In Date" class="date"/></td>
        </tr>

        <tr>
          <td><input name="checkOutDateHotel" value="Check Out Date" class="date"/></td>
        </tr>

    </table>
</div>

<div class="RestaurantField">
    <table>
        <tr>
          <td><select name="restaurantType" id="restaurantType">
        <option value="">- Select Restaurant Type - </option>
            <option>Meal</option>
            <option>Take Away</option>
          </select></td>
        </tr>

        <tr>
          <td><select name="restaurant" id="restaurant">
        <option value="">- Select Restaurant - </option>
          </select></td>
        </tr>

        <tr>
          <td><input name="noOfPeopleRestaurant" type="text" value="Number of People"></td>
        </tr>

        <tr>
          <td><input type="text" class="validate[required,custom[onlyNumber]]" name="dateRestaurant"  value="Booking date" class="date"/></td>
        
        </tr>
    <!-- / TR -->

    <!-- TR -->
    <tr>
          <td><select name="timeRestaurant" id="timeRestaurant">
                                <option value="" selected="selected">- Select Booking Time -</option>
                                <option>00:00</option>
                                <option>01:00</option>
                                <option>02:00</option>
                                <option>03:00</option>
                                <option>04:00</option>
                                <option>05:00</option>
                                <option>06:00</option>
                                <option>07:00</option>
                                <option>08:00</option>
                                <option>09:00</option>
                                <option>10:00</option>
                                <option>11:00</option>
                                <option>12:00</option>
                                <option>13:00</option>
                                <option>14:00</option>
                                <option>15:00</option>
                                <option>16:00</option>
                                <option>17:00</option>
                                <option>18:00</option>
                                <option>19:00</option>
                                <option>20:00</option>
                                <option>21:00</option>
                                <option>22:00</option>
                                <option>23:00</option>
                            </select></td>
        </tr>
    </table>
</div>
</div></td></tr>

	<!-- TR -->
        <tr style="height:26px;">
          <td><input type="button" name="button" id="buttonBooking" value="Submit Your Booking" /></td>
          <td colspan="2"></td>
        </tr>
	<!-- / TR -->

      </table>
       <div id="popupContainerBooking" class="hidden popupContainer">
        <a id="closeBooking" class="hidden close" title="close popup"></a>
        <h1>Confirm your booking</h1>
        <p id="contactAreaLogin" class="contactArea">
            
            
            <table  style="margin:auto;" cellspacing="5" cellpadding="5">
                <tr>
        <td><input type="checkbox" name="sendContactDetails" id="sendContactDetails"/><label for="sendContactDetails">Send my contact details to the selected company so they can contact me regarding the confirmation of this booking.</label></td>
                </tr>
                
                <tr>
        <td><input type="submit" value="Confirm My Booking" class="submitButton"></td>
                </tr>
            </table>
        
        </p>
    </div>
    <div id="overlayEffectBooking" class="overlayEffect">
    </div>
	</form>
	<span class="BusField TrainField PlaneField DoctorField RestaurantField HotelRoomField Field defaultField" id="blah"></span>
</div>


<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript">
$(".date").datepicker({ minDate: 0});
</script>
<?php $this->load->view('footer'); ?>
