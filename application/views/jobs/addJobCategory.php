<?php $this->load->view('header'); 
?>



<div id="container"> 
	
<h1>Add Job</h1>

<?php echo validation_errors(); ?>
<?php
	$counter=0;
	$selectData["none"]="- Please Select -";
	$selectSubData["none"]="- Please Select -";
	while($counter<sizeof($category)){
		$selectData[$category[$counter]->id]=$category[$counter]->name;
		$counter++;	
	} 

	echo form_open();
?>
<table>
<!-- TR -->
<tr>
	<td> Select Parent Category</td>
	<td>
	<?php	echo form_dropdown('parentCategory', $selectData,'1','id="parentCategory"'); ?>
	</td>
</tr>
<tr>
	<td>
		Sub Category
	</td>
	<td>
		<div id="subCat">
			<?php	echo form_dropdown('subCategory', $selectSubData,'id="subCategory"') . "<br />"; ?>
		</div>
	</td>
</tr>
<tr>
	<td>
		Job Title
	</td>
	<td>
		<?php echo form_input('jobTitle',set_value('jobTitle')); ?>
	</td>
</tr>
<tr>
	<td>
		Expiry Date
	</td>
	<td>
		<?php echo form_input('expiryDate',set_value('expiryDate'),'class="date"'); ?>
	</td>
</tr>
<tr>
	<td>
		Tags ( seprate with comma)
	</td>
	<td>
		<?php echo form_input('tags',set_value('tags')); ?>
	</td>
</tr>
	<td>
		Type of Job
	</td>
	<td>
		<span id="type">
<?php	
	echo form_radio('type', '0') . ' Scanned';
	echo "<br />".form_radio('type', '1') . ' Manual';
?>
		</span>
	</td>
</tr>
</table>
<div style="display: none;" id="manual">
<!-- MANUAL -->
	<table>
		<tr>
			<td>
			Description
			</td>
			<td>
<?php echo form_input('jobDescription',set_value('jobDescription')); ?>	
			</td>
		</tr>
		<tr>
			<td>
				Role
			</td>
			<td>
<?php echo form_input('role',set_value('role')); ?>
			</td>	
		</tr>
		<tr>
                        <td>
                                Salary
                        </td>
                        <td>
			From:<?php echo form_input('salaryFrom',set_value('salaryFrom')); ?><br />To:<?php echo form_input('salaryTo',set_value('salaryTo')); ?>
                        </td>
		</tr>
		<tr>
                        <td>
                                Locations
                        </td>
                        <td>
			<?php echo form_input('locations',set_value('locations')); ?>
                        </td>
		</tr>
		<tr>
                        <td>
                                Employer Types
                        </td>
                        <td>
                        <?php echo form_input('employerTypes',set_value('employerTypes')); ?>
                        </td>
                </tr>
	</table>
</div>
<div style="display: none;" id="scanned">
	<table>
		<tr>
			<td>
			Visual Description
			</td>
			<td>
<?php echo form_upload('jobPicture',set_value('jobPicture')); ?>	
			</td>
		</tr>
	</table>
</div>

<?php

	echo "<br />".form_submit('mysubmit', 'Submit Post!'); 
	echo form_close();

?>


</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#parentCategory').change(function(){
			$('#subCategory').attr("disabled","disabled");
			var val = $(this).val();
			$.post("<?php echo base_url('index.php/jobs/showSubCategory'); ?>",{'parentCategory':val},function(data){
				$('#subCat').html(data);
			});
		});
	});
</script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript">
$(".date").datepicker({ minDate: 0});
</script>
<?php $this->load->view('footer'); ?>
