<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Online Resume Generator</title>
	<base href="<?php echo base_url(); ?>"/>
	<link href="css/default.css" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<link rel="stylesheet" type="text/css" href="css/jqtransformplugin/jqtransform.css" />
	<link rel="stylesheet" type="text/css" href="css/formValidator/validationEngine.jquery.css" />
	<link href='http://fonts.googleapis.com/css?family=Concert+One' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="css/jqtransformplugin/jquery.jqtransform.js"></script>
	<script type="text/javascript" src="css/formValidator/jquery.validationEngine.js"></script>

<script type="text/javascript">

	$('.top960 span').click(function(){
		window.location = "<?php echo base_url(); ?>";
	}); 

</script>
<style>
.jcarousel-skin-tango .jcarousel-container {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
   border-radius: 10px;
    background: transparent;
    border: none;
}

.jcarousel-skin-tango .jcarousel-container-horizontal {
    width: 500px;
    padding: 15px 40px;
}

.jcarousel-skin-tango .jcarousel-clip-horizontal {
    width:  500px;
    height: 75px;
}
.jcarousel-skin-tango .jcarousel-next-horizontal {
	top:35px !important;
}
.jcarousel-skin-tango .jcarousel-prev-horizontal {
	top:35px !important;
}
</style>
</head>

<body>


<!-- Top bar -->
<div class="topBar">
	<div class="top960">
		<span class="logo"><a href="<?php echo site_url(); ?>">Online Resume Generator</a></span>
		<div class="searchBox">
			<span><em>Hey, <?php echo $username;?>! (<a href="<?php echo base_url("index.php/user/logout"); ?>">Logout</a>)</em></span>
		</div>
		
	</div>
</div>
<!-- Top bar -->

<div class="topNavBar">
	<div class="top960">
		<ul>
			<li><a href="">Home</a></li>
	 <li><a href="<?php echo site_url('member/basicInfo/basic');?>">Basic Information</a></li>
<li><a href="<?php echo site_url('member/viewInfo/edu');?>">Education</a></li>
	 <li><a href="<?php echo site_url('member/viewInfo/awards');?>">Honors and Awards</a></li>
<li><a href="<?php echo site_url('member/viewInfo/hobbies');?>">Hobbies</a></li>
 <li><a href="<?php echo site_url('member/viewInfo/skills');?>">Skills</a></li>
<li><a href="<?php echo site_url('member/viewInfo/courses');?>">Courses</a></li>
<li><a href="<?php echo site_url('member/getPDF');?>">Get Resume</a></li>




</ul>


		<div class="loginButton" id="buttonLogin"><div><a href="<?php echo base_url("index.php/user/logout"); ?>">Logout</a></div></div>
	</div>
</div>

