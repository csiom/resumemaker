<?php $this->load->view('header'); ?>



<div id="container"> 
	<h1>Users Area</h1>

	<?php if($confirmed != 1) { ?>
	<span class="alert red">Some features of our website requires authenticated users. Please Confirm your Email Address . <a href="<?php echo site_url("/user/logout"); ?>">Click here.</a></span>
	<script language="javascript">
	setTimeout("top.location.href = 'http://www.devplace.in/index.php/user/logout'",5000);
	</script>
	<?php }else {?>	
	<span class="alert green">Welcome back, <?php echo $username;?>! Lets hear from you! &ndash; <a href="<?php echo site_url("/member/basicInfo/basic"); ?>">Click here to update your latest information</a></span>

	<br/><br/>

	<div style="width:972px; overflow:auto;">
		<div style="width:49%; float:left; border:1px #888 solid; padding:1px;">
			<span style="display:inline-block; width:98%; padding: 10px 0px 10px 10px; background: -webkit-gradient(linear, left top, left bottom, from(#999999), to(#ccc));
background: -moz-linear-gradient(top, #999, #ccc); color:white;">News</span>
			<span style="display:inline-block; width:98%; padding: 2% 0px 2% 2%; background:#f9f9f9; font-size:12px; height:200px;"> 
Top News and Software version updates goes here..
</span>
		</div>

		<div style="width:49%; float:right; border:1px #888 solid; padding:1px;">
			<span style="display:inline-block; width:98%; padding: 10px 0px 10px 10px; background: -webkit-gradient(linear, left top, left bottom, from(#999999), to(#ccc));
background: -moz-linear-gradient(top, #999, #ccc); color:white;">Information..</span>
			<span style="display:inline-block; width:98%; padding: 2% 0px 2% 2%; background:#f9f9f9; font-size:12px;  height:200px;"> User's Information Overview goes here ..</span>
		</div>
	</div>

	<div style="width:968px; border:1px #888 solid; padding:1px; margin-top:15px;">
		<span style="display:inline-block; width:99%; padding: 10px 0px 10px 10px; background: -webkit-gradient(linear, left top, left bottom, from(#999999), to(#ccc));
background: -moz-linear-gradient(top, #999, #ccc); color:white;">User &ndash; Resume Sub-Profiles</span>
		<span style="display:inline-block; width:98%; padding: 2% 0px 2% 2%; background:#f9f9f9; font-size:12px;"> Rotating Image Gallery of Latest X number of products.</span>
	</div>
	<br/><br/><br/><br/>
</div>


<?php } $this->load->view('footer'); ?>
