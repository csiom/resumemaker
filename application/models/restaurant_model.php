<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Restaurant_model extends CI_Model {

	var $name = "";
	var $contactPerson = "";
	var $email = "";
	var $resType = "";
    var $title = "";
    var $description = "";

	function __construct(){
		parent::__construct();
    }

    function getRestaurants(){
        $this->load->database();
        $query = $this->db->get('booking_restaurant');
        return $query->result();
    }

    function insertRestaurant(){
    	$this->load->database();
    	$this->name = $_POST['name'];
    	$this->contactPerson = $_POST['contactPerson'];
    	$this->email = $_POST['email'];
    	$this->resType = serialize($_POST['resType']);
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];

		$query = $this->db->insert('booking_restaurant',$this);
		return $query;
    }

    function getRestaurantByID($id){
        $this->load->database();
        $query = $this->db->get_where('booking_restaurant',array('id'=>$id));
        return $query->row();
    }

    function updateRestaurant(){
        $this->load->database();
        $this->name = $_POST['name'];
        $this->contactPerson = $_POST['contactPerson'];
        $this->email = $_POST['email'];
        $this->resType = serialize($_POST['resType']);
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];

        $query = $this->db->update('booking_restaurant',$this,array('id'=>$_POST['id']));
        return $query;
    }

    function getRestaurantsByMatch($type){
        $this->load->database();
        if($type != "-1")
            $query = $this->db->like('resType', $type);
        else 
            $query = $this->db;

        $query = $query->get_where('booking_restaurant');
        return $query->result();
    }

    	
}
?>
