<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jobs_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
    	}

    	function getJobCategory($type=1,$parentId=0){
		//$this->load->library('session');
		
		$query="Select * from jobs_category where type=$type";
		if(!$parentId==null){
		$query.=" AND parentId=$parentId";
		}

		$query = $this->db->query($query);
		return $query->result();
	}
	function saveJob($jobsArray){
		$this->db->insert('jobs', $jobsArray['jobsData']);
		$jobsArray['jobsInformation']['id']=mysql_insert_id();
		$this->db->insert('jobs_information',$jobsArray['jobsInformation']);

	}
}
?>
