<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hotel_model extends CI_Model {

	var $name = "";
	var $contactPerson = "";
	var $email = "";
	var $location = "";
    var $title = "";
    var $description = "";
    var $hotelType = "";

	function __construct(){
		parent::__construct();
    }

    function getHotels(){
        $this->load->database();
        $query = $this->db->get('booking_hotel');
        return $query->result();
    }

    function insertHotel(){
    	$this->load->database();
    	$this->name = $_POST['name'];
    	$this->contactPerson = $_POST['contactPerson'];
    	$this->email = $_POST['email'];
    	$this->location = serialize($_POST['location']);
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];
        $this->hotelType = serialize($_POST['hotelType']);

		$query = $this->db->insert('booking_hotel',$this);
		return $query;
    }

    function getHotelByID($id){
        $this->load->database();
        $query = $this->db->get_where('booking_hotel',array('id'=>$id));
        return $query->row();
    }

    function updateHotel(){
        $this->load->database();
        $this->name = $_POST['name'];
        $this->contactPerson = $_POST['contactPerson'];
        $this->email = $_POST['email'];
        $this->location = serialize($_POST['location']);
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];
        $this->hotelType = serialize($_POST['hotelType']);

        $query = $this->db->update('booking_hotel',$this,array('id'=>$_POST['id']));
        return $query;
    }

    function getHotelsByMatch($hotelType, $location){
        $this->load->database();
        if($hotelType != "-1")
            $query = $this->db->like('hotelType', $hotelType);
        else 
            $query = $this->db;

        if($location != "-1")
            $query = $query->like('location', $location);

        $query = $query->get_where('booking_hotel');
        return $query->result();
    }


    	
}
?>
