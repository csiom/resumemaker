<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bus_model extends CI_Model {

	var $name = "";
	var $contactPerson = "";
	var $email = "";
    var $title = "";
    var $description = "";

	function __construct(){
		parent::__construct();
    }

    function getBus(){
        $this->load->database();
        $query = $this->db->get('booking_bus_company');
        return $query->result();
    }

    function insertBus(){
    	$this->load->database();
    	$this->name = $_POST['name'];
    	$this->contactPerson = $_POST['contactPerson'];
    	$this->email = $_POST['email'];
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];

		$query = $this->db->insert('booking_bus_company',$this);
		return $query;
    }

    function getBusByID($id){
        $this->load->database();
        $query = $this->db->get_where('booking_bus_company',array('id'=>$id));
        return $query->row();
    }

    function updateBus(){
        $this->load->database();
        $this->name = $_POST['name'];
        $this->contactPerson = $_POST['contactPerson'];
        $this->email = $_POST['email'];
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];

        $query = $this->db->update('booking_bus_company',$this,array('id'=>$_POST['id']));
        return $query;
    }

    	
}
?>
