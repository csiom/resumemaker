<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Location_model extends CI_Model {

	var $name = "";

	function __construct(){
		parent::__construct();
    }

    function getLocations(){
    	$this->load->database();

		$query = $this->db->get('location');
		return $query->result();
    }

	function getLocationsByOrder($key,$value){
    	$this->load->database();

		$query = $this->db->order_by($key,$value)->get_where('location');
		return $query->result();
    }

    function getLocationById($id){
    	$this->load->database();
    	$query = $this->db->get_where('location',array('id'=>$id));
    	return $query->row();
    }

    function insertLocation(){
    	$this->load->database();
    	$this->name = $_POST['name'];
    	$query = $this->db->insert('location',$this);
    	return $query;
    }

    function updateLocation(){
    	$this->name = $_POST['name'];
    	$this->load->database();
    	$query = $this->db->update('location',$this,array('id'=>$_POST['id']));
    	return $query;
    }
    	
}
?>
