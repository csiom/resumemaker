<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Booking_model extends CI_Model {

	var $name = "";
	var $contactPerson = "";
	var $email = "";

	function __construct(){
	parent::__construct();
    	}

    	function getBusCompanies(){
		$this->load->database();
		$query = $this->db->get('booking_bus_company');
		return $query->result();
	
	}

	function getHospitals(){
		$this->load->database();
		$query = $this->db->get('booking_hospital');
		return $query->result();
	
	}

	function getRestaurants(){
		$this->load->database();
		$query = $this->db->get('booking_restaurant');
		return $query->result();
	
	}

	function getHotels(){
		$this->load->database();
		$query = $this->db->get('booking_hotel');
		return $query->result();
	
	}

	function getHotelTypes(){
		$this->load->database();
		$query = $this->db->get('booking_hotel_type');
		return $query->result();
	
	}

	

	function getTypeOverview($value){
		$this->load->database();
		$query = $this->db->get_where('booking_type',array('id'=>$value));
		if ($query->num_rows() > 0)
			return $query->row();
		else{
			$query = $this->db->get_where('booking_type',array('id'=>'default'));
			return $query->row();
		}
	}

	function getCompanyOverview($table,$where){
		$this->load->database();
		$query = $this->db->get_where($table,$where);
		if ($query->num_rows() > 0)
			return $query->row();
		else{
			$query = $this->db->get_where('booking_type',array('id'=>'default'));
			return $query->row();
		}
	}
}
?>
