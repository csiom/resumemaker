<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hospital_model extends CI_Model {

	var $name = "";
	var $contactPerson = "";
	var $email = "";
    var $title = "";
    var $description = "";

	function __construct(){
		parent::__construct();
    }

    function getHospital(){
        $this->load->database();
        $query = $this->db->get('booking_hospital');
        return $query->result();
    }

    function insertHospital(){
    	$this->load->database();
    	$this->name = $_POST['name'];
    	$this->contactPerson = $_POST['contactPerson'];
    	$this->email = $_POST['email'];
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];

		$query = $this->db->insert('booking_hospital',$this);
		return $query;
    }

    function getHospitalByID($id){
        $this->load->database();
        $query = $this->db->get_where('booking_hospital',array('id'=>$id));
        return $query->row();
    }

    function updateHospital(){
        $this->load->database();
        $this->name = $_POST['name'];
        $this->contactPerson = $_POST['contactPerson'];
        $this->email = $_POST['email'];
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];

        $query = $this->db->update('booking_hospital',$this,array('id'=>$_POST['id']));
        return $query;
    }

    	
}
?>
