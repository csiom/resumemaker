<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hoteltype_model extends CI_Model {

	var $name = "";

	function __construct(){
		parent::__construct();
    }

    function getHotelTypes(){
    	$this->load->database();

		$query = $this->db->get('booking_hotel_type');
		return $query->result();
    }

	function getHotelTypesByOrder($key,$value){
    	$this->load->database();

		$query = $this->db->order_by($key,$value)->get_where('booking_hotel_type');
		return $query->result();
    }

    function getHotelTypeById($id){
    	$this->load->database();
    	$query = $this->db->get_where('booking_hotel_type',array('id'=>$id));
    	return $query->row();
    }

    function insertHotelType(){
    	$this->load->database();
    	$this->name = $_POST['name'];
    	$query = $this->db->insert('booking_hotel_type',$this);
    	return $query;
    }

    function updateHotelType(){
    	$this->name = $_POST['name'];
    	$this->load->database();
    	$query = $this->db->update('booking_hotel_type',$this,array('id'=>$_POST['id']));
    	return $query;
    }
    	
}
?>
